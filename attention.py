import torch
import numpy as np
import torch.nn as nn
import argparse
from train import load_embed_mat, load_vocab, load_dataset, train, evaluate
from torch.optim import Adam


class RNNModel(nn.Module):
    def __init__(self, embed_mat, rnn_model, hidden_size, num_layers, dropout, bidirectional, activ_fun=torch.relu,
                 use_attention=False):
        super().__init__()

        self.embed_mat = embed_mat

        self.rnn1 = rnn_model(input_size=300, hidden_size=hidden_size, num_layers=num_layers, batch_first=False,
                              dropout=dropout, bidirectional=bidirectional)
        self.rnn2 = rnn_model(input_size=(hidden_size * 2 if bidirectional else hidden_size), hidden_size=hidden_size,
                              num_layers=num_layers, batch_first=False, dropout=dropout, bidirectional=bidirectional)

        if use_attention:
            self.W1 = nn.Parameter(torch.randn(hidden_size // 2, hidden_size))
            self.w2 = nn.Parameter(torch.randn(hidden_size // 2))

            self.fc1 = nn.Linear((hidden_size if bidirectional else hidden_size // 2), hidden_size, bias=True)
        else:
            self.fc1 = nn.Linear((2 * hidden_size if bidirectional else hidden_size), hidden_size, bias=True)

        self.fc2 = nn.Linear(hidden_size, 1, bias=True)

        self.activ_fun = activ_fun

        self.use_attention = use_attention

    def forward(self, x):
        x = self.embed_mat(x)
        y = x.transpose(1, 0)

        # y - all hidden states from last layer
        # (h_n, c_n) - hidden/cell state for each layer at t = seq_len
        # implicitly (default): e.g. (h_0, c_0) = (torch.zeros(2,10,150), torch.zeros(2,10,150))
        y, hc = self.rnn1(y)
        y, hc = self.rnn2(y, hc)

        if self.use_attention:
            h = y.permute(1, 0, 2)  # batch_size, seq_len, num_features
            h_attn = torch.bmm(h, self.W1.T.expand(h.shape[0], -1, -1))
            a = self.w2 * torch.tanh(h_attn)
            alpha = torch.softmax(a, dim=1)

            attn = torch.sum(alpha * h_attn, dim=1)

            y = attn
        else:
            # input to decoder = output of last layer (layer 2) from last RNN cell
            y = y[-1, ...]

        y = self.fc1(y)
        y = self.activ_fun(y)
        y = self.fc2(y)

        return y


class BaselineModel(nn.Module):
    def __init__(self, embed_mat, activ_fun=torch.relu):
        super().__init__()

        self.embed_mat = embed_mat
        self.fc1 = nn.Linear(300, 150, bias=True)
        self.fc2 = nn.Linear(150, 150, bias=True)
        self.fc3 = nn.Linear(150, 1, bias=True)

        self.activ_fun = activ_fun

    def forward(self, x):
        x = self.embed_mat(x)
        y = torch.mean(x, dim=1, keepdim=False)

        y = self.fc1(y)
        y = self.activ_fun(y)
        y = self.fc2(y)
        y = self.activ_fun(y)
        y = self.fc3(y)
        return y


def trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val,
                 test_dataset, batch_size_test, optimiz=Adam):
    criterion = nn.BCEWithLogitsLoss()
    optimizer = optimiz(model.parameters(), lr=lr)

    for epoch in range(1, epochs + 1):
        print()
        print()
        print("################ EPOCH {}: ################".format(epoch))
        print("------ TRAIN ------")
        train(model, train_dataset, batch_size_train, optimizer, criterion, grad_clip)
        print()
        print("------ VAL ------")
        evaluate(model, valid_dataset, batch_size_val, criterion)

    print()
    print("************** TEST **************")
    evaluate(model, test_dataset, batch_size_test, criterion)


def initialize_model(text_vocab, rnn_type, hidden_size=150, num_layers=2, dropout=0, bidirectional=False,
                     pretrained=True, activ_fun=torch.relu, use_attention=False):
    print("rnn_type = {}, hidden_size = {}, num_layers = {}, dropout = {}, bidirectional = {}".format(rnn_type,
                                                                                                      hidden_size,
                                                                                                      num_layers,
                                                                                                      dropout,
                                                                                                      bidirectional))
    embed_mat = load_embed_mat(text_vocab, pretrained=pretrained)

    return RNNModel(embed_mat, rnn_type, hidden_size, num_layers, dropout, bidirectional, activ_fun=activ_fun,
                    use_attention=use_attention)


def initialize_baseline_model(text_vocab, pretrained=True, activ_fun=torch.relu):
    print("baseline model")
    embed_mat = load_embed_mat(text_vocab, pretrained=pretrained)

    return BaselineModel(embed_mat, activ_fun=activ_fun)


def main(args, rnn_types=[nn.RNN, nn.GRU, nn.LSTM]):
    seed = int(args.seed)
    max_size = int(args.maxsize)
    min_freq = int(args.minfreq)
    epochs = int(args.epochs)
    lr = float(args.lr)
    batch_size_train, batch_size_val, batch_size_test = int(args.bs_train), int(args.bs_val), int(args.bs_test)
    grad_clip = float(args.grad_clip) if args.grad_clip else None
    use_attention = True if int(args.use_attention)==1 else False

    np.random.seed(seed)
    torch.manual_seed(seed)

    ##############

    text_vocab, label_vocab = load_vocab("data/sst_train_raw.csv", max_size, min_freq)

    train_dataset, valid_dataset, test_dataset = load_dataset("data/sst_train_raw.csv", "data/sst_valid_raw.csv",
                                                              "data/sst_test_raw.csv", text_vocab, label_vocab)

    print("***************** BASELINE *****************")
    baseline_model = initialize_baseline_model(text_vocab, pretrained=False)
    trainValTest(baseline_model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)

    print()
    print("======== ATTENTION =", use_attention)
    for rnn_type in rnn_types:
        print()
        print("***************** RNN type:", rnn_type, "*****************")
        model = initialize_model(text_vocab, rnn_type, pretrained=False, use_attention=use_attention)
        trainValTest(model, lr, epochs, train_dataset, batch_size_train, grad_clip, valid_dataset, batch_size_val, test_dataset, batch_size_test)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed")
    parser.add_argument("--maxsize")
    parser.add_argument("--minfreq")
    parser.add_argument("--epochs")
    parser.add_argument("--lr")
    parser.add_argument("--bs_train")
    parser.add_argument("--bs_val")
    parser.add_argument("--bs_test")
    parser.add_argument("--grad_clip")
    parser.add_argument("--use_attention")

    return parser.parse_args()


if __name__ == "__main__":
    main(parse_args())
