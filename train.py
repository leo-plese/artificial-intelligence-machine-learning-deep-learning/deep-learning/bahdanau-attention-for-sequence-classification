import torch
import numpy as np
import torch.nn as nn
from load_data import Vocab, get_word_and_label_frequencies, NLPDataset, pad_collate_fn, get_embed_mat_wrapped, \
    get_embed_mat
from torch.utils.data import DataLoader
from math import ceil



def train(model, train_dataset, batch_size, optimizer, criterion, grad_clip=None):
    train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True, collate_fn=pad_collate_fn)

    model.train()
    train_loss = 0
    for batch_num, batch in enumerate(train_loader):
        model.zero_grad()

        x, y = batch[0], batch[1]

        logits = model(x)
        loss = criterion(logits.flatten(), torch.FloatTensor(y))

        train_loss += loss.item()

        loss.backward()

        if grad_clip:
            nn.utils.clip_grad_norm(model.parameters(), grad_clip)

        optimizer.step()

    train_loss /= ceil(len(train_dataset) / batch_size)
    print("loss =", train_loss)


def evaluate(model, val_dataset, batch_size, criterion):
    val_loader = DataLoader(dataset=val_dataset, batch_size=batch_size, shuffle=True, collate_fn=pad_collate_fn)

    model.eval()
    with torch.no_grad():
        val_loss = 0

        y_list, gt_y_list = [], []
        cnt_correct = 0
        for batch_num, batch in enumerate(val_loader):
            x, y = batch[0], batch[1]

            logits = model(x)
            Y_ = torch.FloatTensor(y)
            loss = criterion(logits.flatten(), Y_)

            val_loss += loss.item()

            y_pred = torch.sigmoid(logits.flatten()) >= 0.5

            cnt_correct += (y_pred == Y_).sum()

            y_list.append(y_pred)
            gt_y_list.append(Y_)

        val_loss /= ceil(len(val_dataset) / batch_size)
        print("loss =", val_loss)

        preds = torch.cat(y_list, dim=0)
        gts = torch.cat(gt_y_list, dim=0)

        accuracy, recall, precision, f1 = eval_perf_binary(preds.detach().numpy(), gts.detach().numpy())
        print("accuracy =", accuracy)
        print("score", cnt_correct.item() / len(val_dataset))
        print("recall =", recall)
        print("precision =", precision)
        print("f1 =", f1)


def load_vocab(train_ds_filename, max_size, min_freq):
    word_frequencies, label_frequencies = get_word_and_label_frequencies(train_ds_filename)
    text_vocab = Vocab(frequencies=word_frequencies, max_size=max_size, min_freq=min_freq, txt_vocab=True)
    label_vocab = Vocab(frequencies=label_frequencies, max_size=max_size, min_freq=min_freq, txt_vocab=False)

    return text_vocab, label_vocab


def load_embed_mat(text_vocab, pretrained):
    return get_embed_mat_wrapped(get_embed_mat(vocab=text_vocab, embed_mat_filename="data/sst_glove_6b_300d.txt"),
                                 pretrained)


def load_dataset(ds_train_filename, ds_val_filename, ds_test_filename, text_vocab, label_vocab):
    train_dataset = NLPDataset(ds_train_filename, text_vocab, label_vocab)
    valid_dataset = NLPDataset(ds_val_filename, text_vocab, label_vocab)
    test_dataset = NLPDataset(ds_test_filename, text_vocab, label_vocab)

    return train_dataset, valid_dataset, test_dataset



########## EVAL METRICS ##########
def get_accuracy(tp, tn, N):
    return (tp + tn) / N


def get_recall(tp, fn):
    return tp / (tp + fn)


def get_precision(tp, fp):
    if (tp + fp) == 0:
        return None
    return tp / (tp + fp)


def get_f1(precision, recall):
    return (2 * precision * recall) / (precision + recall)


# actual x predicted
# classes: from 0 to C-1 (top to bottom (predicted), left to right (actual))
def get_confusion_matrix(Y, Y_, C):
    conf_mat = np.zeros((C, C))
    for i in range(C):
        Y_pred_i = Y[np.where(Y_ == i)]

        for x in Y_pred_i:
            conf_mat[i][x] += 1

    conf_mat = conf_mat.T

    return conf_mat


# c_ind - class index
# classes: 0, 1 (top to bottom (predicted), left to right (actual))
def get_conf_mat_for_class(multi_conf_mat, c_ind, C):
    tp = multi_conf_mat[c_ind][c_ind]
    tn = sum([multi_conf_mat[i][j] if i != c_ind and j != c_ind else 0 for i in range(C) for j in range(C)])
    fp = sum([multi_conf_mat[c_ind][i] if i != c_ind else 0 for i in range(C)])
    fn = sum([multi_conf_mat[i][c_ind] if i != c_ind else 0 for i in range(C)])

    conf_mat = np.array([[tn, fn], [fp, tp]])

    return conf_mat


def get_tp_tn_fp_fn_binary(Y, Y_):
    eq = (Y == Y_)
    neq = (Y != Y_)

    y0 = (Y == 0)
    y1 = (Y == 1)

    tp = np.sum(np.logical_and(eq, y1))
    tn = np.sum(np.logical_and(eq, y0))
    fp = np.sum(np.logical_and(neq, y1))
    fn = np.sum(np.logical_and(neq, y0))

    return tp, tn, fp, fn


def eval_perf_binary(Y, Y_):
    tp, tn, fp, fn = get_tp_tn_fp_fn_binary(Y, Y_)

    print("conf mat =")
    print(np.array([[tn, fn], [fp, tp]]))
    accuracy = get_accuracy(tp, tn, len(Y))
    recall = get_recall(tp, fn)
    precision = get_precision(tp, fp)

    if precision and recall:
        f1 = get_f1(precision, recall)
    else:
        f1 = None

    return accuracy, recall, precision, f1



