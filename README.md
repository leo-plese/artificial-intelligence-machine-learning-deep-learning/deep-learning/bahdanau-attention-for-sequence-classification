Bahdanau Attention for Sequence Classification. Implemented in Python using NumPy, PyTorch and Matplotlib library.

My lab assignment in Deep Learning, FER, Zagreb.

Task description in "TaskSpecification.pdf" in section "Bonus zadatak: pozornost (max 20% bodova)".

Docs in "sadrzaj.txt"

Created: 2021

